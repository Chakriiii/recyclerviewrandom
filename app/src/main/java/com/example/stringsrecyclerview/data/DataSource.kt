package com.example.stringsrecyclerview.data

class DataSource {


    fun randomString(length: Int): String {

        val chars = "qwertyuioplkjhgfdsazxcvbnmMNBVCXZASDFGHJKLPOIUYTREWQ"
        return (1..length).map { chars.random() }.joinToString(separator = "")
    }

    fun main(length: Int, number: Int): List<String> {
        var i = 0
        val r = mutableListOf<String>()
        while (i < number) {
            i++
            r += randomString(length)
        }
        return r
    }

    fun main2(length: Int, number: Int): List<String> {

        return (0..number).map { randomString(length) }

    }
}